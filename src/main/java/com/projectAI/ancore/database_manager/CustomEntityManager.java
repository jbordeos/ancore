package com.projectAI.ancore.database_manager;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.projectAI.ancore.model.GenericEntity;
import com.projectAI.ancore.service.LogConstant;

public class CustomEntityManager {
	Context ctx;
	String strDbName;

	final String TAG = this.getClass().getSimpleName();

	public CustomEntityManager(Context ctx) {
		this.ctx = ctx;
		this.strDbName = ctx.getClass().getSimpleName();
	}

	public CustomEntityManager(Context ctx, String strDbName) {
		this.ctx = ctx;
		this.strDbName = strDbName;
	}

	public <T extends GenericEntity<? extends Serializable>> Long persist(
			T entity) {
		Field[] fields = entity.getClass().getFields();
		String strTblName = entity.getClass().getSimpleName();
		SQLiteDatabase db = null;
		try {
			db = ctx.openOrCreateDatabase(strDbName, Context.MODE_PRIVATE, null);
			if (isTblNull(strTblName, db)) {
				String strTblFields = generateTblFields(fields);
				db.execSQL("CREATE TABLE IF NOT EXISTS " + strTblName + " ("
						+ strTblFields + ");");
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "CREATE TABLE IF NOT EXISTS " + strTblName
							+ " (" + strTblFields + ");");
				}
			}
			ContentValues values = new ContentValues();
			for (Field field : fields) {
				if (!field.getName().equals("id")) {
					field.setAccessible(true);
					values.put(field.getName(), field.get(entity) + "");
				}
			}
			long result = db.insert(strTblName, null, values);
			if (result != -1) {
				db.close();
				return result;
			}
		} catch (Exception e) {
			if (LogConstant.LOGGABLE) {
				e.printStackTrace();
			}
		} finally {
			if (db != null)
				db.close();
		}
		return Long.valueOf(-1);
	}

	public <T extends GenericEntity<? extends Serializable>> boolean remove(
			T entity) {
		String strTblName = entity.getClass().getSimpleName();
		SQLiteDatabase db = null;
		try {
			db = ctx.openOrCreateDatabase(strDbName, Context.MODE_PRIVATE, null);
			int result = db.delete(strTblName, "id = ?",
					new String[] { entity.getId() + "" });
			if (result > 0) {
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "Remove success!");
				}
				db.close();
				return true;
			}

		} catch (Exception e) {
			if (LogConstant.LOGGABLE) {
				e.printStackTrace();
			}
		} finally {
			if (db != null)
				db.close();
		}
		return false;
	}

	public <T extends GenericEntity<? extends Serializable>> void removeTable(
			Class<T> clazz) {
		String strTblName = clazz.getSimpleName();
		SQLiteDatabase db = null;
		try {
			db = ctx.openOrCreateDatabase(strDbName, Context.MODE_PRIVATE, null);
			db.execSQL("DROP TABLE IF EXISTS " + strTblName + ";");
		} catch (Exception e) {
			if (LogConstant.LOGGABLE) {
				e.printStackTrace();
			}
		} finally {
			if (db != null)
				db.close();
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends GenericEntity<? extends Serializable>> ArrayList<T> customQuery(
			Class<T> clazz, String query) {
		ArrayList<T> listEntity = null;
		Field[] fields = clazz.getFields();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try {
			db = ctx.openOrCreateDatabase(strDbName, Context.MODE_PRIVATE, null);
			cursor = db.rawQuery(query, null);
			cursor.moveToFirst();
			if (cursor != null && cursor.getCount() != 0) {
				listEntity = new ArrayList<T>();
				Object entity = null;
				do {
					entity = clazz.newInstance();
					if (LogConstant.LOGGABLE) {
						Log.d(TAG, "" + entity);
					}
					bindCursorDataToEntity(cursor, (T) entity, fields);
					listEntity.add((T) entity);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			if (LogConstant.LOGGABLE) {
				e.printStackTrace();
			}
		} finally {
			if (db != null)
				db.close();
			if (cursor != null) {
				cursor.close();
				cursor = null;
			}
		}
		return listEntity;
	}

	@SuppressWarnings("unchecked")
	public <T extends GenericEntity<? extends Serializable>> ArrayList<T> findAll(
			Class<T> clazz) {
		ArrayList<T> listEntity = null;
		Field[] fields = clazz.getFields();
		String strTblName = clazz.getSimpleName();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try {
			db = ctx.openOrCreateDatabase(strDbName, Context.MODE_PRIVATE, null);
			cursor = db.rawQuery("SELECT * FROM " + strTblName, null);
			cursor.moveToFirst();
			if (cursor != null && cursor.getCount() != 0) {
				listEntity = new ArrayList<T>();
				Object entity = null;
				do {
					entity = clazz.newInstance();
					if (LogConstant.LOGGABLE) {
						Log.d(TAG, "" + entity);
					}
					bindCursorDataToEntity(cursor, (T) entity, fields);
					listEntity.add((T) entity);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			if (LogConstant.LOGGABLE) {
				e.printStackTrace();
			}
		} finally {
			if (db != null)
				db.close();
			if (cursor != null) {
				cursor.close();
				cursor = null;
			}
		}
		return listEntity;
	}

	@SuppressWarnings("unchecked")
	public <T extends GenericEntity<? extends Serializable>> T find(
			Class<T> clazz, Serializable id) {
		Field[] fields = clazz.getFields();
		Object entity = null;
		String strTblName = clazz.getSimpleName();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try {
			db = ctx.openOrCreateDatabase(strDbName, Context.MODE_PRIVATE, null);
			cursor = db.rawQuery("SELECT * FROM " + strTblName + " WHERE ID = "
					+ id, null);
			cursor.moveToFirst();
			if (cursor != null && cursor.getCount() != 0) {
				entity = clazz.newInstance();
				bindCursorDataToEntity(cursor, (T) entity, fields);
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "binding success! ");
				}
			}
		} catch (Exception e) {
			if (LogConstant.LOGGABLE) {
				e.printStackTrace();
			}
		} finally {
			if (db != null)
				db.close();
			if (cursor != null) {
				cursor.close();
				cursor = null;
			}
		}
		if (LogConstant.LOGGABLE) {
			Log.d(TAG, "find(Entity): " + entity);
		}
		return (T) entity;
	}

	public <T extends GenericEntity<? extends Serializable>> boolean contain(
			Class<T> clazz, Serializable id) {
		String strTblName = clazz.getSimpleName();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try {
			db = ctx.openOrCreateDatabase(strDbName, Context.MODE_PRIVATE, null);
			cursor = db.rawQuery("SELECT * FROM " + strTblName + " WHERE ID = "
					+ id, null);
			cursor.moveToFirst();
			if (cursor != null && cursor.getCount() != 0) {
				if (LogConstant.LOGGABLE) {
					db.close();
				}
				cursor.close();
				return true;
			}
		} catch (Exception e) {
			if (LogConstant.LOGGABLE) {
				e.printStackTrace();
			}
		} finally {
			if (db != null)
				db.close();
			if (cursor != null) {
				cursor.close();
				cursor = null;
			}

		}
		return false;
	}

	private <T extends GenericEntity<? extends Serializable>> T bindCursorDataToEntity(
			Cursor cursor, T entity, Field[] fields)
			throws IllegalAccessException {
		for (Field field : fields) {
			String fieldType = field.getType().toString();
			if (LogConstant.LOGGABLE) {
				Log.d(TAG,
						"field type: "
								+ fieldType
								+ " "
								+ cursor.getString(cursor
										.getColumnIndexOrThrow(field.getName()))
								+ " entity:" + entity);
			}
			if (fieldType.equals("class java.lang.String")) {
				field.set(entity, cursor.getString(cursor
						.getColumnIndexOrThrow(field.getName())));
			} else if (fieldType.equals("int")
					|| fieldType.equals("class java.lang.Integer")) {
				field.set(entity, cursor.getInt(cursor
						.getColumnIndexOrThrow(field.getName())));
			} else if (fieldType.equals("float")
					|| fieldType.equals("class java.lang.Float")) {
				field.set(entity, cursor.getFloat(cursor
						.getColumnIndexOrThrow(field.getName())));
			} else if (fieldType.equals("double")
					|| fieldType.equals("class java.lang.Double")) {
				field.set(entity, cursor.getDouble(cursor
						.getColumnIndexOrThrow(field.getName())));
			} else if (fieldType.equals("long")
					|| fieldType.equals("class java.lang.Long")) {
				field.set(entity, cursor.getLong(cursor
						.getColumnIndexOrThrow(field.getName())));
			} else if (fieldType.equals("char")
					|| fieldType.equals("class java.lang.Character")) {
				field.setChar(
						entity,
						cursor.getString(
								cursor.getColumnIndexOrThrow(field.getName()))
								.toCharArray()[0]);
			} else if (fieldType.equals("boolean")
					|| fieldType.equals("class java.lang.Boolean")) {
				field.setBoolean(entity,
						Boolean.valueOf(cursor.getString(cursor
								.getColumnIndexOrThrow(field.getName()))));
			} else if (fieldType.equalsIgnoreCase("class java.util.Date")) {
				field.set(entity, Date.parse(cursor.getString(cursor
						.getColumnIndexOrThrow(field.getName()))));
			} else if (fieldType.equalsIgnoreCase("class [B")) {
				field.set(entity, (cursor.getBlob(cursor
						.getColumnIndexOrThrow(field.getName()))));
				Log.d(TAG, "blob data: " + cursor.getBlob(cursor
						.getColumnIndexOrThrow(field.getName())) + " : " + field.getName() + " : " + cursor.getCount());
			}
		}
		return entity;
	}

	public <T extends GenericEntity<? extends Serializable>> boolean merge(
			T entity) {
		Field[] fields = entity.getClass().getFields();
		String strTblName = entity.getClass().getSimpleName();
		SQLiteDatabase db = null;

		try {
			db = ctx.openOrCreateDatabase(strDbName, Context.MODE_PRIVATE, null);
			// check if entity table is already created.
			if (isTblNull(strTblName, db)) {
				String strTblFields = generateTblFields(fields);
				db.execSQL("CREATE TABLE IF NOT EXISTS " + strTblName + " ("
						+ strTblFields + ");");
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "CREATE TABLE IF NOT EXISTS " + strTblName
							+ " (" + strTblFields + ");");
				}
			}
			ContentValues values = new ContentValues();
			Long id = Long.valueOf("-1");
			for (Field field : fields) {
				if (!field.getName().equals("id")) {
					field.setAccessible(true);
					values.put(field.getName(), field.get(entity) + "");
				} else {
					id = (Long) field.get(entity);
				}
			}
			int result = db.update(strTblName, values, "id = ?",
					new String[] { "" + id });
			if (result > 0) {
				db.close();
				return true;
			}
		} catch (Exception e) {
			if (LogConstant.LOGGABLE) {
				e.printStackTrace();
			}
		} finally {
			if (db != null)
				db.close();
		}
		return false;
	}

	private boolean isTblNull(String tblName, SQLiteDatabase db) {
		Cursor cursor = db.rawQuery("SELECT CASE WHEN tbl_name = '" + tblName
				+ "' THEN 1 ELSE 0 END FROM sqlite_master WHERE tbl_name = '"
				+ tblName + "' AND type = 'table'", null);
		if (LogConstant.LOGGABLE) {
			Log.d(TAG, "isTblNotNull? " + cursor.getCount());
		}
		switch (cursor.getCount()) {
		case 0:
			cursor.close();
			return true;
		case 1:
			cursor.close();
			return false;
		default:
			cursor.close();
			return true;
		}
	}

	private String generateTblFields(Field[] fields) {
		String strTblFields = "";
		boolean bool = false;
		for (Field field : fields) {
			if (LogConstant.LOGGABLE) {
				Log.d(TAG, "field: " + field.getName() + " type: "
						+ getDBType("" + field.getType()));
			}
			if (!bool) {
				bool = true;
				if (field.getName().equals("id")) {
					if (LogConstant.LOGGABLE) {
						Log.d(TAG, "id: " + field.getName());
					}
					strTblFields += field.getName() + " "
							+ getDBType("" + field.getType())
							+ " PRIMARY KEY autoincrement";
				} else
					strTblFields += field.getName() + " "
							+ getDBType("" + field.getType());
			} else {
				if (field.getName().equals("id")) {
					strTblFields += ", " + field.getName() + " "
							+ getDBType("" + field.getType())
							+ " PRIMARY KEY autoincrement";
				} else
					strTblFields += ", " + field.getName() + " "
							+ getDBType("" + field.getType());
			}
		}
		return strTblFields;
	}

	private String getDBType(String fieldType) {
		String type = "VARCHAR(45)";
		if (fieldType.equals("class java.lang.String"))
			type = "VARCHAR(45)";
		else if (fieldType.equals("char"))
			type = "CHARACTER(20)";
		else if (fieldType.equals("float")
				|| fieldType.equals("class java.lang.Float"))
			type = "FLOAT";
		else if (fieldType.equals("int")
				|| fieldType.equals("class java.lang.Integer")
				|| fieldType.equals("long")
				|| fieldType.equals("class java.lang.Long"))
			type = "INTEGER";
		else if (fieldType.equals("boolean")
				|| fieldType.equals("class java.lang.Boolean"))
			type = "BOOLEAN";
		else if (fieldType.equalsIgnoreCase("class java.util.Date"))
			type = "DATE";
		else if (fieldType.equalsIgnoreCase("class [B"))
			type = "BLOB";
		return type;
	}

}

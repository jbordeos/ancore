package com.projectAI.ancore.service;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.projectAI.ancore.model.GenericEntity;

public class CustomParser {
	private final static String TAG = "CustomParser";

	public static <T extends GenericEntity<? extends Serializable>> T parseJSONToEntity(
			Class<T> clazz, String jsonString) throws Exception {

		JSONObject object = new JSONObject(jsonString);
		JSONObject objectAttr = object.getJSONObject(clazz.getSimpleName()
				.toLowerCase());
		if (LogConstant.LOGGABLE) {
			Log.d(TAG, "id -" + objectAttr.getString("id"));
		}

		T entity = clazz.newInstance();
		Field[] fields = clazz.getFields();
		entity = bindJSONDataToEntity(objectAttr, entity, fields);
		return entity;
	}

	public static <T extends GenericEntity<? extends Serializable>> ArrayList<T> parseFromJSONToEntityList(
			Class<T> clazz, String jsonString) throws Exception {
		JSONArray objectArray = new JSONArray(jsonString);
		ArrayList<T> listEntity = new ArrayList<T>();
		T entity = null;
		Field[] fields = clazz.getFields();

		for (int i = 0; i < objectArray.length(); i++) {
			JSONObject subObject = objectArray.getJSONObject(i);
			JSONObject subObjectAttr = subObject.getJSONObject(clazz
					.getSimpleName().toLowerCase());

			if (LogConstant.LOGGABLE) {
				Log.d(TAG, "id-" + subObjectAttr.getString("id"));
			}

			entity = clazz.newInstance();
			entity = bindJSONDataToEntity(subObjectAttr, entity, fields);

			listEntity.add(entity);
		}
		return listEntity;
	}

	public static <T extends GenericEntity<? extends Serializable>> ArrayList<T> parseJSONToListEntityWOArrayName(
			Class<T> clazz, String jsonString) throws Exception {
		JSONArray objectArray = new JSONArray(jsonString);
		ArrayList<T> listEntity = new ArrayList<T>();
		T entity = null;
		Field[] fields = clazz.getFields();
		for (int i = 0; i < objectArray.length(); i++) {
			JSONObject subObjectAttr = objectArray.getJSONObject(i);

			try {
				entity = clazz.newInstance();
				entity = bindJSONDataToEntity(subObjectAttr, entity, fields);
			} catch (Exception e) {
				if (LogConstant.LOGGABLE) {
					e.printStackTrace();
					Log.d(TAG, "Error message - " + e.getMessage());
				}
			}

			listEntity.add(entity);
		}
		if (LogConstant.LOGGABLE) {
			Log.d("JSON", jsonString);
		}
		return listEntity;
	}

	private static <T extends GenericEntity<? extends Serializable>> T bindJSONDataToEntity(
			JSONObject jsonObject, T entity, Field[] fields)
			throws JSONException, IllegalAccessException {
		for (Field field : fields) {
			if (LogConstant.LOGGABLE) {
				Log.d(TAG,
						field.getName() + "-" + jsonObject.get(field.getName()));
			}

			String fieldType = field.getType().toString();
			if (LogConstant.LOGGABLE) {
				Log.d(TAG, "Datatype: " + fieldType);
			}
			if (fieldType.equals("class [B")) {
				field.set(entity,
						(jsonObject.get(field.getName()) + "").getBytes());
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "parse as: class [B");
				}
			}
			if (fieldType.equals("class [Ljava.lang.String;")) {
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "Field Type - " + field.getType());
				}
				JSONArray objectArray = (JSONArray) jsonObject.get(field
						.getName());
				String[] strArray = new String[objectArray.length()];

				for (int i = 0; i < objectArray.length(); i++) {
					strArray[i] = objectArray.getString(i);
					if (LogConstant.LOGGABLE) {
						Log.d(TAG,
								"Object array value - "
										+ objectArray.getString(i));
					}
				}
				field.set(entity, strArray);
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "parse as: class [Ljava.lang.String;");
				}
			}
			if (fieldType.equals("class java.lang.String")) {
				field.set(entity, jsonObject.getString(field.getName()));
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "parse as: class java.lang.String");
				}
			}
			if (fieldType.equals("double")) {
				field.setDouble(entity,
						Double.valueOf(jsonObject.get(field.getName()) + ""));
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "parse as: double");
				}
			}
			if (fieldType.equals("int")) {
				field.setInt(entity,
						Integer.valueOf(jsonObject.get(field.getName()) + ""));
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "parse as: int");
				}
			}
			if (fieldType.equals("class java.lang.Integer")) {
				field.set(entity,
						Integer.valueOf(jsonObject.get(field.getName()) + ""));
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "parse as: class java.lang.Integer");
				}
			}
			if (fieldType.equals("class java.lang.Long")) {
				field.set(entity,
						Long.valueOf(jsonObject.get(field.getName()) + ""));
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "parse as: class java.lang.Long");
				}
			}
			if (fieldType.equals("long")) {
				field.setLong(entity,
						Long.valueOf(jsonObject.get(field.getName()) + ""));
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "parse as: long");
				}
			}
		}
		return entity;
	}
}

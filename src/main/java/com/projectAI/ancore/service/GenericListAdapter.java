package com.projectAI.ancore.service;

import java.lang.reflect.Field;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GenericListAdapter<T> extends BaseAdapter {
	private final String TAG = getClass().getSimpleName();

	private Context context;
	private ArrayList<T> listItems = new ArrayList<T>();
	private int[] fieldFrom;
	private String[] sFieldFrom;
	private LayoutInflater mInflater;
	private int resLayoutRow;
	private int[] resTo;
	private boolean isStringFieldFrom = false;

	private int[] colors = null;

	public GenericListAdapter(Context context, ArrayList<T> listItems,
			int resLayoutRow, int[] resTo, int[] fieldFrom) {
		this.context = context;
		this.resTo = resTo;
		this.resLayoutRow = resLayoutRow;
		this.listItems = listItems;
		this.fieldFrom = fieldFrom;
		mInflater = LayoutInflater.from(context);
	}

	public GenericListAdapter(Context context, ArrayList<T> listItems,
			int resLayoutRow, int[] resTo, int[] fieldFrom, int[] altColor) {
		this.context = context;
		this.resTo = resTo;
		this.resLayoutRow = resLayoutRow;
		this.listItems = listItems;
		this.fieldFrom = fieldFrom;
		this.colors = altColor;
		mInflater = LayoutInflater.from(context);
	}

	public GenericListAdapter(Context context, ArrayList<T> listItems,
			int resLayoutRow, int[] resTo, String[] sFieldFrom) {
		this.context = context;
		this.resTo = resTo;
		this.resLayoutRow = resLayoutRow;
		this.listItems = listItems;
		this.sFieldFrom = sFieldFrom;
		mInflater = LayoutInflater.from(context);
		isStringFieldFrom = true;
	}

	public GenericListAdapter(Context context, ArrayList<T> listItems,
			int resLayoutRow, int[] resTo, String[] sFieldFrom, int[] altColor) {
		this.context = context;
		this.resTo = resTo;
		this.resLayoutRow = resLayoutRow;
		this.listItems = listItems;
		this.sFieldFrom = sFieldFrom;
		mInflater = LayoutInflater.from(context);
		isStringFieldFrom = true;
		this.colors = altColor;
	}

	public int getCount() {
		return listItems.size();
	}

	public Object getItem(int position) {
		return listItems.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup viewGroup) {
		if (convertView == null) {
			convertView = mInflater.inflate(resLayoutRow, null);
		}
		Field[] fields;
		if (!isStringFieldFrom) {
			for (int i = 0; i < fieldFrom.length; i++) {
				try {
					fields = listItems.get(position).getClass().getFields();
					fields[fieldFrom[i]].setAccessible(true);
					if (LogConstant.LOGGABLE) {
						Log.d(TAG,
								position
										+ " - "
										+ convertView.findViewById(resTo[i])
												.getClass()
										+ " - "
										+ fields[fieldFrom[i]].get(listItems
												.get(position)) + " - "
										+ (i + 1) + " - " + listItems.size()
										+ " : "
										+ fields[fieldFrom[i]].getName());
					}
					if (convertView.findViewById(resTo[i]).getClass()
							.toString().equals("class android.widget.TextView")) {
						TextView tv = (TextView) convertView
								.findViewById(resTo[i]);
						tv.setText(""
								+ fields[fieldFrom[i]].get(listItems
										.get(position)));
					}
					if (convertView.findViewById(resTo[i]).getClass()
							.toString().equals("class android.widget.EditText")) {
						EditText et = (EditText) convertView
								.findViewById(resTo[i]);
						et.setText(""
								+ fields[fieldFrom[i]].get(listItems
										.get(position)));

					}
					if (convertView.findViewById(resTo[i]).getClass()
							.toString()
							.equals("class android.widget.ImageView")) {
						ImageView iv = (ImageView) convertView
								.findViewById(resTo[i]);
						byte[] data = (byte[]) fields[fieldFrom[i]]
								.get(listItems.get(position));
						Bitmap bmp = BitmapFactory.decodeByteArray(data, 0,
								data.length);
						iv.setImageBitmap(bmp);
					}
				} catch (Exception e) {
					if (LogConstant.LOGGABLE) {
						e.printStackTrace();
					}
				}
			}
		} else {
			try {
				fields = listItems.get(position).getClass().getFields();
				for (int x = 0; x < fields.length; x++) {
					fields[x].setAccessible(true);
					for (int y = 0; y < sFieldFrom.length; y++) {
						if (fields[x].getName().equals(sFieldFrom[y])) {
							if (convertView.findViewById(resTo[y]).getClass()
									.toString()
									.equals("class android.widget.TextView")) {
								TextView tv = (TextView) convertView
										.findViewById(resTo[y]);
								tv.setText(""
										+ fields[x].get(listItems.get(position)));
							}
							if (convertView.findViewById(resTo[y]).getClass()
									.toString()
									.equals("class android.widget.EditText")) {
								EditText et = (EditText) convertView
										.findViewById(resTo[y]);
								et.setText(""
										+ fields[x].get(listItems.get(position)));

							}
							if (convertView.findViewById(resTo[y]).getClass()
									.toString()
									.equals("class android.widget.ImageView")) {
								try {
									ImageView iv = (ImageView) convertView
											.findViewById(resTo[y]);
									byte[] data = (byte[]) fields[x]
											.get(listItems.get(position));
									Bitmap bmp = BitmapFactory.decodeByteArray(
											data, 0, data.length);
									iv.setImageBitmap(bmp);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			} catch (SecurityException e) {
				if (LogConstant.LOGGABLE) {
					e.printStackTrace();
				}
			} catch (IllegalArgumentException e) {
				if (LogConstant.LOGGABLE) {
					e.printStackTrace();
				}
			} catch (IllegalAccessException e) {
				if (LogConstant.LOGGABLE) {
					e.printStackTrace();
				}
			}
		}
		if (colors != null) {
			int colorPos = position % colors.length;
			convertView.setBackgroundColor(colors[colorPos]);
		}
		return convertView;
	}

	private static class ViewHolder {
		ArrayList<TextView> listTV = new ArrayList<TextView>();

		public ViewHolder(int size, Context context) {
			for (int i = 0; i < size; i++) {
				TextView tv = new TextView(context);
				tv.setPadding(10, 0, 0, 0);
				ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.WRAP_CONTENT, 1);
				tv.setLayoutParams(layoutParams);
				listTV.add(tv);
			}
		}
	}
}

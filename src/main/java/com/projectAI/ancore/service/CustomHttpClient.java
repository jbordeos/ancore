package com.projectAI.ancore.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Base64;
import android.util.Log;

/**
 * Created by IntelliJ IDEA. User: joel Date: 8/3/11 Time: 10:47 AM To change
 * this template use File | Settings | File Templates.
 */
public class CustomHttpClient {

	public static String username = "joel.bordoes@gmail.com";
	public static String password = "wojako";
	/** The time it takes for our client to timeout */
	public static final int HTTP_TIMEOUT = 30 * 1000; // milliseconds

	/** Single instance of our HttpClient */
	// private static HttpClient mHttpClient;

	/**
	 * Get our single instance of our HttpClient object.
	 * 
	 * @return an HttpClient object with connection parameters set
	 */

	public static int executeHTTPDelete(String url) throws Exception {
		BufferedReader in = null;
		int statusCode = -1;

		try {
			DefaultHttpClient client = new DefaultHttpClient();

			HttpDelete request = new HttpDelete(url);
			HttpResponse response = client.execute(request);

			statusCode = response.getStatusLine().getStatusCode();
			if (LogConstant.LOGGABLE) {
				Log.d("REQUEST CODE:", statusCode + "");
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					if (LogConstant.LOGGABLE) {
						e.printStackTrace();
					}
				}
			}
		}
		return statusCode;
	}

	public static int executeHTTPPut(String url,
			ArrayList<NameValuePair> postParameters) throws Exception {
		BufferedReader in = null;
		int statusCode = -1;

		try {
			DefaultHttpClient client = new DefaultHttpClient();

			HttpPut request = new HttpPut(url);
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(
					postParameters);
			request.setEntity(formEntity);
			HttpResponse response = client.execute(request);

			statusCode = response.getStatusLine().getStatusCode();
			if (LogConstant.LOGGABLE) {
				Log.d("REQUEST CODE:", statusCode + "");
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					if (LogConstant.LOGGABLE) {
						e.printStackTrace();
					}
				}
			}
		}
		return statusCode;
	}

	public static int executeHTTPPost(String url,
			ArrayList<NameValuePair> postParameters) throws Exception {
		BufferedReader in = null;
		int statusCode = -1;

		try {
			DefaultHttpClient client = new DefaultHttpClient();

			HttpPost request = new HttpPost(url);
			// request.addHeader("Authorization", "Basic "+ getCredentials());
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(
					postParameters);
			request.setEntity(formEntity);
			HttpResponse response = client.execute(request);

			statusCode = response.getStatusLine().getStatusCode();
			if (LogConstant.LOGGABLE) {
				Log.d("REQUEST CODE:", statusCode + "");
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					if (LogConstant.LOGGABLE) {
						e.printStackTrace();
					}
				}
			}
		}
		return statusCode;
	}

	public static int executeHTTPPostForStartUp(String url) throws Exception {
		BufferedReader in = null;
		int statusCode = -1;

		try {
			DefaultHttpClient client = new DefaultHttpClient();

			HttpPost request = new HttpPost(url);
			HttpResponse response = client.execute(request);
			String content = convertStreamToString(
					response.getEntity().getContent()).trim();
			statusCode = Integer.valueOf(content);
			if (LogConstant.LOGGABLE) {
				Log.d("REQUEST CODE:", statusCode + "");
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					if (LogConstant.LOGGABLE) {
						e.printStackTrace();
						Log.d("REQUEST CODE:", e.getMessage());
					}
				}
			}
		}
		return statusCode;
	}

	/**
	 * Performs an HTTP GET request to the specified url.
	 * 
	 * @param url
	 *            The web address to post the request to
	 * @return The result of the request
	 * @throws Exception
	 */
	public static String executeHttpGet(String url) throws Exception {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);

		httpGet.addHeader("Authorization", "Basic " + getCredentials());
		HttpResponse response;

		try {
			response = httpClient.execute(httpGet);
			if (LogConstant.LOGGABLE) {
				Log.i("LOG", "Status:[" + response.getStatusLine().toString()
						+ "]");
			}

			HttpEntity httpEntity = response.getEntity();
			if (httpEntity != null) {
				InputStream inputStream = httpEntity.getContent();
				String result = convertStreamToString(inputStream);
				if (LogConstant.LOGGABLE) {
					Log.i("Log", "Result of converstion: [" + result + "]");
				}
				inputStream.close();
				return result;
			}
		} catch (ClientProtocolException e) {
			if (LogConstant.LOGGABLE) {
				Log.e("REST", "There was a protocol based error", e);
			}
		} catch (IOException e) {
			if (LogConstant.LOGGABLE) {
				Log.e("REST", "There was an IO Stream related error", e);
			}
		}
		return null;
	}

	private static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			if (LogConstant.LOGGABLE) {
				e.printStackTrace();
			}
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				if (LogConstant.LOGGABLE) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}

	private static String getCredentials() {
		String cred = null;

		try {
			cred = Base64.encodeToString(
					(username + ":" + password).getBytes("UTF-8"),
					android.util.Base64.NO_WRAP);
			if (LogConstant.LOGGABLE) {
				Log.d("Http", "BasicAuth: " + cred);
			}

		} catch (UnsupportedEncodingException e) {
			if (LogConstant.LOGGABLE) {
				e.printStackTrace();
			}
		}
		return cred;
	}

	public static InputStream getDataFromUrl(String src) {
		try {
			URL url = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			return input;
		} catch (IOException e) {
			if (LogConstant.LOGGABLE) {
				e.printStackTrace();
			}
			return null;
		}
	}
}

package com.projectAI.ancore.service;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;

import com.projectAI.ancore.dao.GenericEntityDao;
import com.projectAI.ancore.dao.GenericEntityDaoImpl;
import com.projectAI.ancore.model.GenericEntity;

public class GenericEntityServiceImpl implements GenericEntityService {
	Context ctx;
	String strDbName;
	GenericEntityDao genericEntityDao;

	public GenericEntityServiceImpl(Context ctx) {
		this.ctx = ctx;
		this.strDbName = ctx.getClass().getSimpleName();
	}

	public GenericEntityServiceImpl(Context ctx, String strDbName) {
		this.ctx = ctx;
		this.strDbName = strDbName;
	}

	public <T extends GenericEntity<? extends Serializable>> Long create(
			T entity) {
		if (genericEntityDao == null)
			genericEntityDao = new GenericEntityDaoImpl(ctx, strDbName);
		return genericEntityDao.create(entity);
	}

	public <T extends GenericEntity<? extends Serializable>> boolean update(
			T entity) {
		if (genericEntityDao == null)
			genericEntityDao = new GenericEntityDaoImpl(ctx, strDbName);
		return genericEntityDao.update(entity);
	}

	public <T extends GenericEntity<? extends Serializable>> boolean delete(
			T entity) {
		if (genericEntityDao == null)
			genericEntityDao = new GenericEntityDaoImpl(ctx, strDbName);
		return genericEntityDao.delete(entity);
	}

	public <T extends GenericEntity<? extends Serializable>> T get(
			Class<T> clazz, Serializable id) {
		if (genericEntityDao == null)
			genericEntityDao = new GenericEntityDaoImpl(ctx, strDbName);
		return (T) genericEntityDao.get(clazz, id);
	}

	public <T extends GenericEntity<? extends Serializable>> ArrayList<T> findAll(
			Class<T> clazz) {
		if (genericEntityDao == null)
			genericEntityDao = new GenericEntityDaoImpl(ctx, strDbName);
		return genericEntityDao.findAll(clazz);
	}

	public <T extends GenericEntity<? extends Serializable>> boolean contain(
			Class<T> clazz, Serializable id) {
		if (genericEntityDao == null)
			genericEntityDao = new GenericEntityDaoImpl(ctx, strDbName);
		return this.genericEntityDao.contain(clazz, id);
	}

	public <T extends GenericEntity<? extends Serializable>> ArrayList<T> sampleCustomQuery(
			String query, Class<T> clazz) {
		if (genericEntityDao == null)
			genericEntityDao = new GenericEntityDaoImpl(ctx, strDbName);
		return this.genericEntityDao.sampleCustomQuery(query, clazz);
	}
}

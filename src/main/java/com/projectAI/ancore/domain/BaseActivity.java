package com.projectAI.ancore.domain;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.projectAI.ancore.service.LogConstant;

public abstract class BaseActivity extends Activity {

	private LinearLayout linBaseBody;
	private LinearLayout linBaseFooter;
	private LinearLayout linHeader;
	private LayoutInflater layoutInflater;
	private Button btnSave, btnNext, btnPrev, btnCancel;

	private final String TAG = getClass().getSimpleName();
	private ProgressDialog mProgressDialog;

	public void onCreate(Bundle savedInstanceState) {
		if(LogConstant.LOGGABLE){
			Log.d(TAG, "onCreate");
		}
		super.onCreate(savedInstanceState);
	}

	protected void showLoadingProgressDialog(Context context) {
		if(LogConstant.LOGGABLE){
			Log.d(TAG, "showing loading progress dialog");
		}

		if (mProgressDialog == null)
			mProgressDialog = new ProgressDialog(context);
		mProgressDialog.setMessage("Loading. Please wait...");
		mProgressDialog.show();
	}

	protected void showProgressDialog(String message, Context context) {
		if(LogConstant.LOGGABLE){
			Log.d(TAG, "showing progress dialog");
		}
		if (mProgressDialog == null)
			mProgressDialog = new ProgressDialog(context);
		mProgressDialog.setMessage(message);
		mProgressDialog.show();
	}

	protected void dismissProgressDialog() {
		if(LogConstant.LOGGABLE){
			Log.d(TAG, "dismissing progress dialog");
		}
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
	}

	protected void logException(Exception e) {
		if(LogConstant.LOGGABLE){
			Log.e(TAG, e.getMessage(), e);
		}

		Writer result = new StringWriter();
		e.printStackTrace(new PrintWriter(result));
	}

	protected void displayNetworkError() {
		Toast toast = Toast.makeText(this,
				"A problem occurred with the network connection.",
				Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	protected void displayAuthorizationError() {
		Toast toast = Toast
				.makeText(
						this,
						"You are not authorized. Please sign out and reauthorize the app.",
						Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	protected Context getContext() {
		return this;
	}

	public void toast(String string) {
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
	}

}

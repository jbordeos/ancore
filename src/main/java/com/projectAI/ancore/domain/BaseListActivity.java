package com.projectAI.ancore.domain;

import java.io.Serializable;
import java.util.ArrayList;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.projectAI.ancore.model.GenericEntity;
import com.projectAI.ancore.service.LogConstant;

public abstract class BaseListActivity<T extends GenericEntity<? extends Serializable>>
		extends ListActivity {
	private final String TAG = getClass().getSimpleName();
	public String jsonResult;
	public ArrayList<T> entityList = null;
	private ProgressDialog progressDialog = null;

	protected abstract void bindDataToList(ArrayList<T> data);

	protected abstract void addActionToList();

	protected abstract ArrayList<T> getEntityListFromJson(String jsonString);

	protected abstract String getRequestResult();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			new ProgressTask(this, this.getApplicationContext()).execute();
		} catch (Exception e) {
			if (LogConstant.LOGGABLE) {
				Log.v(TAG, e.toString());
			}
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	private class ProgressTask extends AsyncTask<String, Void, Boolean> {
		private Context ctx;

		public ProgressTask(ListActivity lActivity, Context ctx) {
			this.ctx = ctx;
			progressDialog = new ProgressDialog(lActivity);
		}

		@Override
		protected void onPreExecute() {
			progressDialog.setMessage("Processing...");
			progressDialog.show();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				jsonResult = getRequestResult();
				return true;
			} catch (Exception e) {
				if (LogConstant.LOGGABLE) {
					Log.e(TAG, "Getting request from " + params[0] + " failed.");
				}
				return false;
			}
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			if (success) {
				try {
					entityList = getEntityListFromJson(jsonResult);
					bindDataToList(entityList);
					addActionToList();
				} catch (Exception e) {
					if (LogConstant.LOGGABLE) {
						e.printStackTrace();
						Log.d(TAG, e.getMessage());
					}
				}
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "Internet connection: Good.");
				}
			} else {
				if (LogConstant.LOGGABLE) {
					Log.d(TAG, "Internet connection: Bad / Request URL: Wrong");
				}
				Toast.makeText(ctx, "No Internet Connection",
						Toast.LENGTH_SHORT);
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
				progressDialog = null;
			}
		}
	}
}

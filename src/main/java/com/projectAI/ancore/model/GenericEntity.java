package com.projectAI.ancore.model;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: joel
 * Date: 9/7/11
 * Time: 2:07 PM
 * To change this template use File | Settings | File Templates.
 */
public interface GenericEntity<T extends Serializable> extends Serializable {
    T getId();
    void setId(T id);
}

package com.projectAI.ancore.model;

/**
 * Created by IntelliJ IDEA.
 * User: joel
 * Date: 9/7/11
 * Time: 2:41 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class LongPkEntity extends AbstractEntity<Long> {

	private static final long serialVersionUID = -9046478083185984700L;

	public Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

package com.projectAI.ancore.model;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: joel
 * Date: 9/7/11
 * Time: 3:09 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractEntity<T extends Serializable> implements GenericEntity<T> {

	private static final long serialVersionUID = -6150150059029828841L;

}

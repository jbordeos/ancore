package com.projectAI.ancore.model;

/**
 * Created by IntelliJ IDEA.
 * User: joel
 * Date: 9/7/11
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class StringPkEntity extends AbstractEntity<String> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

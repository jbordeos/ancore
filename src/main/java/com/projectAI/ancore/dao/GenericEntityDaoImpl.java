package com.projectAI.ancore.dao;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;

import com.projectAI.ancore.database_manager.CustomEntityManager;
import com.projectAI.ancore.model.GenericEntity;

public class GenericEntityDaoImpl implements GenericEntityDao {
	Context ctx;
	String strDbName;
	CustomEntityManager entityManager;

	public GenericEntityDaoImpl(Context ctx) {
		this.ctx = ctx;
		this.strDbName = ctx.getClass().getSimpleName();
	}

	public GenericEntityDaoImpl(Context ctx, String strDbName) {
		this.ctx = ctx;
		this.strDbName = strDbName;
	}

	public <T extends GenericEntity<? extends Serializable>> Long create(
			T entity) {
		if (entityManager == null)
			entityManager = new CustomEntityManager(ctx, strDbName);
		return this.entityManager.persist(entity);
	}

	public <T extends GenericEntity<? extends Serializable>> boolean update(
			T entity) {
		if (entityManager == null)
			entityManager = new CustomEntityManager(ctx, strDbName);
		return this.entityManager.merge(entity);
	}

	public <T extends GenericEntity<? extends Serializable>> boolean delete(
			T entity) {
		if (entityManager == null)
			entityManager = new CustomEntityManager(ctx, strDbName);
		return this.entityManager.remove(entity);
	}

	public <T extends GenericEntity<? extends Serializable>> T get(
			Class<T> clazz, Serializable id) {
		if (entityManager == null)
			entityManager = new CustomEntityManager(ctx, strDbName);
		return this.entityManager.find(clazz, id);
	}

	public <T extends GenericEntity<? extends Serializable>> ArrayList<T> findAll(
			Class<T> clazz) {
		if (entityManager == null)
			entityManager = new CustomEntityManager(ctx, strDbName);
		return this.entityManager.findAll(clazz);
	}

	public <T extends GenericEntity<? extends Serializable>> boolean contain(
			Class<T> clazz, Serializable id) {
		if (entityManager == null)
			entityManager = new CustomEntityManager(ctx, strDbName);
		return this.entityManager.contain(clazz, id);
	}

	// --------------------------------------------DO NOT
	// MODIFY--------------------------------------------------//

	public <T extends GenericEntity<? extends Serializable>> ArrayList<T> sampleCustomQuery(
			String query, Class<T> clazz) {
		if (entityManager == null)
			entityManager = new CustomEntityManager(ctx, strDbName);
		return entityManager.customQuery(clazz, query);
	}
}

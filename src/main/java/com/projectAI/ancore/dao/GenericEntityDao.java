package com.projectAI.ancore.dao;

import java.io.Serializable;
import java.util.ArrayList;

import com.projectAI.ancore.model.GenericEntity;

public interface GenericEntityDao {
	<T extends GenericEntity<? extends Serializable>> Long create(T entity);

	<T extends GenericEntity<? extends Serializable>> boolean update(T entity);

	<T extends GenericEntity<? extends Serializable>> boolean delete(T entity);

	<T extends GenericEntity<? extends Serializable>> T get(Class<T> clazz,
			Serializable id);

	<T extends GenericEntity<? extends Serializable>> ArrayList<T> findAll(
			Class<T> clazz);

	<T extends GenericEntity<? extends Serializable>> boolean contain(
			Class<T> clazz, Serializable id);

	<T extends GenericEntity<? extends Serializable>> ArrayList<T> sampleCustomQuery(
			String query, Class<T> clazz);
}
